﻿using Aplykatse.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;

namespace Aplykatse.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            _textBatnu = "TEXTIK";

            PropertyChanged += OnPropertyChanged;

            BatnCommand = new BaseCommand();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand BatnCommand;

        private string _textBatnu;

        public string TextBatnu
        {
            get
            {
                return _textBatnu;
            }
            set
            {
                _textBatnu = value;
                
            }
        }

        private string _textboxik;

        public string Textboxik 
        { 
            get 
            {
                return _textboxik;
            } 
            set 
            {
                _textboxik = value;
            } 
        }


        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }
    }
}
